# Ontologies – statistics, biases, tools, networks, and interpretation

You can find the materials, presentations and exercises inside of each days' folder.


## Announcement

**Date/Time:** 2020-10-26 - 2020-10-29 - 09:00 CET - 12:30 CET
**Location:** Online 
**Categories:** Bio-IT Course, CBNA, Course, R, Workshop

**Cost:** Free of charge
**Teachers:** Bálint Mészáros, Matt Rogon, Renato Alves
[Check your local time](https://arewemeetingyet.com/Berlin/2020-10-26/09:00/Ontologies%20-%20statistics%2C%20biases%2C%20tools%2C%20networks%2C%20and%20interpretation)

### Main aim of the course

During this 4-day online course participants will learn how to access and utilize biological ontologies (mainly Gene Ontology) to arrive at sound biological inferences using standard and customized enrichment analyses and network visualization.

### Course description

Ontologies are the primary means to store biological data about genes, proteins, and other biological entities in an accessible and unambiguous way. These ontologies are hierarchical collections of descriptive terms, typically represented as networks (graphs), serving as a fundamental structure to store biological knowledge. The use of ontology-based enrichment and network representations to infer biological properties of gene and protein sets identified in experiments, is now ubiquitous. Consequently, a solid understanding and fluency in ontology-based approaches is an essential skill in a wide range of biological fields.

#### During the course you will learn how to

* access gene and pathway ontologies to build high-quality maps for data interpretation and visualisation;
* customize query data as well as databases themselves to deal with a wide range of biases and produce reliable results using online tools, Cytoscape, and R packages;
* explore Gene Ontology, KEGG and Reactome databases;
* understand, utilize and create sub-ontologies to address specific biological questions by executing enrichment analyses;
* Use R-based packages to automate, accelerate and create reproducible analyses;
* be able to interpret and assess the quality of GeneOntology analyses in publications by understanding the strengths and limitations of enrichment calculations.

After the course you will be able to independently extract data from public resources, assess their quality, annotate your own networks and biomolecules in Cytoscape, extract data using R, and critically interpret the results.

### Requirements

This course is an introduction to ontologies, however we require:

* working knowledge of Cytoscape (e.g. [EMBL CBNA - introduction to cytoscape and network biology](https://git.embl.de/rogon/introduction-to-cytoscape-and-network-biology))
* basic knowledge of R (data structures, file operations etc.) (e.g. [EMBL CSDA - tidyverse R intro](https://git.embl.de/klaus/tidyverse_R_intro) or [EMBL Bio-IT - Introduction to R](https://bio-it.embl.de/events/introduction-to-r/))

If you have further questions please don't hesitate to contact us.

### Affiliation

This Bio-IT workshop is sponsored by <a href="https://www.denbi.de/">de.NBI</a>, the German Network for Bioinformatics Infrastructure and is the result of a collaboration between [EMBL CBNA](https://bio-it.embl.de/centres/cbna/), the [Gibson Team](https://www.embl.de/research/units/scb/gibson/).

![deNBI logo](https://bio-it.embl.de/wp-content/uploads/2018/08/denbi_transparent-300x66.png")
![CBNA logo](https://bio-it.embl.de/wp-content/uploads/Centres/CBNA_SVG.png)
